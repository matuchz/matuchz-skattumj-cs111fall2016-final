import java.util.*;
import java.util.Scanner;
import java.util.Random;
import java.util.Date;

public class BlackjackMainfinal
  {
    public static void main (String[] args)
    {
      //Variable dictionary
      Scanner scan = new Scanner(System.in);
      Random random = new Random();
      int answer;
      int ace;
      String[] suita = {"Spades", "Hearts", "Diamonds","Clubs"};
      String[] facea = {"Ace of","Two of","Three of","Four of","Five of","Six of","Seven of","Eight of","Nine of","Ten of","Jack of","Queen of","King of"};
      int[] valuea =  {11,2,3,4,5,6,7,8,9,10,10,10,10};
      int cardCt = 0;

      System.out.println("\nZachary Matuch and Jonas Skattum \n" + new Date() + "\nFinal Project");
      System.out.println("\nWelcome to our BlackJack Simulator\n");
            //Starts the actual game


            ArrayList < TheDeck > deck = new ArrayList < >(); // initializing the arrayList
          for ( int suit = 0; suit <= 3; suit++ ) {
             for ( int value = 1; value <= 13; value++ ) {
                deck.add (new TheDeck(facea[value - 1],suita[suit], valuea[value-1], 11));
              }
            }

  System.out.println("Would you like to play a hand? Enter 1 for yes, and 2 for no\n");
  answer = scan.nextInt(); //user input

  while (answer == 1){ //allowing the user to begin playing

  int userhand = 0;
  int dealerhand = 0;

  for (int y = 0; y < 2; y++){ //drawing two cards initially
    int x = random.nextInt(deck.size()); //randomly draws a card from Arraylist
    System.out.println("The dealer gives you a: " +deck.get(x).getFace()+" "+ deck.get(x).getSuit()); //prining the cards
    userhand += deck.get(x).getValue(); // gives total value of your current hand

    if(deck.get(x).getValue()==11){ // giving option of counting ace as 1 or 11
      System.out.println("To have the ace count as a 1, enter 1, and enter 11 for 11");
      ace = scan.nextInt();
      userhand += ace -11; // adding ace value to your current hand
    }
    deck.remove(x); // removes used cards
  }

  System.out.println("Your total is: " +userhand); //prints total value of player hand

  for (int y = 0; y < 1; y++){ // drawing dealers first card
    int x = random.nextInt(deck.size()); // randomly draws card form ArrayList
    System.out.println("\nThe dealer has a: " +deck.get(x).getFace()+" "+ deck.get(x).getSuit()); //prints dealers first card
    dealerhand += deck.get(x).getValue(); //gives total value of dealers hand
    deck.remove(x); // removes used card
    }

  int newHand = 0;
  int next = 0;

    System.out.println("\nWould you like to hit or stand? (Enter 1 for hit, and 0 for stand)"); // asks user to hit or stand
    next = scan.nextInt(); //user input

  while (next == 1){ // user hits begins process of drawing next card

    for (int y = 0; y < 1; y++){
      int x = random.nextInt(deck.size()); //randomly draws card from ArrayList
      System.out.println("The dealer gives you a: " +deck.get(x).getFace()+" "+ deck.get(x).getSuit());
      userhand += deck.get(x).getValue(); //adds value of new card to userhand

      if(deck.get(x).getValue()==11){ //giving option of counting ace as 1 or 11
        System.out.println("To have the ace count as a 1, enter 1, and enter 11 for 11");
        ace = scan.nextInt();
        userhand += ace -11; // adds value of ace to userhand
    }
    deck.remove(x); //removes used card
  }

    if (userhand < 22) // boolean condition to continue or stop
    System.out.println("Your new total is: " +userhand); // prints total value of hand
    else  {
    System.out.println("Your new total is " + userhand + ". You bust\n"); // prits that you bust
    break; // ends process of drawing new cards for player
  }

    System.out.println("\nWould you like to hit or stand? (Enter 1 for hit, and 0 for stand)");
    next = scan.nextInt(); //user input, goes back up to drawing new card
    }

    if (next != 1){ // if user decides to stay, prints next line
      System.out.println("You stay at "+ userhand + "\n");
      }

    while (dealerhand < 17){ // boolean for the dealer to draw new card
    for (int y = 0; y < 1; y++){
    int x = random.nextInt(13); // draws random card from ArrayList
    System.out.println("Dealer hits.\nDealer gets a: " + deck.get(x).getFace()+" "+ deck.get(x).getSuit());
    dealerhand += deck.get(x).getValue(); //gives total value of dealers new hand
    deck.remove(x);

    if (dealerhand < 22) //boolean for dealer to continue drawing cards
    System.out.println("Dealers new total is: " +dealerhand+ "\n"); // prints out dealerhands new value
    else
    System.out.println("Dealer's new total is " + dealerhand + "\nDealer busts\n");
    break; // ends process of drawing new cards for dealer
        }
      }

      if (userhand > dealerhand && userhand <= 21 && dealerhand < 21 || userhand <= 21 && dealerhand > 21) {
        System.out.println("You Win!"); //winning conditions
      }

      else if (userhand == dealerhand && userhand <= 21 || userhand > 21 && dealerhand > 21 ) {
        System.out.println("You Tied!"); // tying conditions
      }

        else
          System.out.println("Dealer Wins!"); //dealer wins if none of the above booleans are satisfied

      System.out.println("Would you like to play another hand? 1 for yes, and 2 for no");
      answer = scan.nextInt(); // allows player to go again or stop playing
        }
        System.out.println("Thanks for playing our BlackJack Simulator. See you next time");

          }
}
