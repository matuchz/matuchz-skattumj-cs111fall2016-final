public class TheDeck{
  private String face, suit;
  private int value, value2;

//constructor
  public TheDeck(String f,String s, int v, int v2)
  {
    face = f;
    suit = s;
    value = v;
    value2 = v2;
  }


  public String getFace() // gets the "face" of the card i.e king, queen, 7, 10 etc
{
  return face;


}

  public int getValue() //gets the actual value of the card (i.e 7=7, King = 10, Queen = 10, Ace = 1, 8 = 8
  {

    return value;

  }
  public int getValue2() // for the ace, ace's value2 will be 11 and will change from value to value2 depending on the other cards

  {

    return value2;

  }

  public String getSuit()// gets the suit of the card, mostly useless for this game
  {

    return suit;


  }

}
